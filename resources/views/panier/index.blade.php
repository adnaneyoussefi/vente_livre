@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr class="table-active">
                    <td>Nom</td>
                    <td>Auteur</td>
                </tr>  
            </thead>
            @foreach($panier["livres"] as $livre)
                <tbody>
                    <tr>
                        <td><span class="text-muted">{{ $livre->nom }}</span></td>
                        <td> {{ $livre->auteur }}
                        </td>
                    </tr>
                </tbody>
                <tbody>
            @endforeach 
        </table>
    </div>
</div>
@endsection
