<?php

namespace App\Http\Controllers;

use App\Livre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PanierController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userId = Auth::user()->id;
        $panier = $request->session()->get('key');
        $arr = array();
        foreach ($panier as &$value) {
            if($value["id"] == $userId)
                $arr = $value;
        }
        return view('panier/index',[
            'panier' => $arr
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $userId = Auth::user()->id;
        $livre = Livre::find($id);
        $panier = $request->session()->get('key');
        function ifUserExist($panier, $userId) {
            if(!is_array($panier))
                return false;
            else {  
                for($i=0; $i<count($panier); $i++) {
                    if($panier[$i]["id"] == $userId)
                        return true;
                }
            }
            return false;
        }
        if(Auth::guest())
            $request->session->get('key');
        else {
            if(ifUserExist($panier, $userId)) {
                for($i=0; $i<count($panier); $i++) {
                    if($panier[$i]["id"] == $userId){
                        array_push($panier[$i]["livres"], $livre);
                        $request->session()->put('key', $panier);
                    }
                }
            }
            else {
                $request->session()->push('key', array("id" => $userId, "livres" => array($livre)));
            }
        }
        return redirect()->route('afficherLivres');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
