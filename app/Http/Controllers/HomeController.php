<?php

namespace App\Http\Controllers;

use App\Livre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        \dump(Auth::guest());
        dump($request->session()->get('key'));
        $livres = Livre::all();
        return view('home', [
            'livres' => $livres
        ]);
    }
}
